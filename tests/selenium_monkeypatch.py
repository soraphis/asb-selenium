from selenium.webdriver.remote.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException

def try_find_xpath(self: WebDriver, xpath: str):
    """ searches for element, if not found returns none"""
    try:
        a = self.find_element_by_xpath(xpath=xpath)
        return a
    except NoSuchElementException:
        pass
    return None

def try_find_linktext(self: WebDriver, linktext: str):
    try:
        a = self.find_element_by_link_text(link_text=linktext)
        return a
    except NoSuchElementException:
        pass
    return None



WebDriver.try_find_xpath = try_find_xpath
del(try_find_xpath)

WebDriver.try_find_linktext = try_find_linktext
del(try_find_linktext)