import unittest
import tests.selenium_monkeypatch
from tests.ASB_constants import student_name

from tests.selenium_utils import get_browser, LoginPageHelper, wait_for_page_load


class RoleApplicationTest(unittest.TestCase):
    def setUp(self):
        self.w = get_browser()
        self.student = student_name


    def test_docentapplication(self):
        w = self.w
        LoginPageHelper.perform_login(self.student)
        wait_for_page_load()

        appl = w.try_find_xpath("//a[contains(text(), '%s')]" % "Dozentenbewerbung")
        assert appl is not None


    def test_tutorapplication(self):
        w = self.w
        LoginPageHelper.perform_login(self.student)
        wait_for_page_load()

        appl = w.try_find_xpath("//a[contains(text(), '%s')]" % "Tutorenbewerbung")
        assert appl is not None

    def test_systemadminapplication(self):
        w = self.w
        LoginPageHelper.perform_login(self.student)
        wait_for_page_load()

        appl = w.try_find_xpath("//a[contains(text(), '%s')]" % "Systembetreuerbewerbung")
        assert appl is not None


