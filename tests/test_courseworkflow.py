import unittest
import tests.selenium_monkeypatch
from tests.ASB_constants import student_name, docent_last_name, docent_name, student_last_name

from tests.selenium_utils import get_browser, LoginPageHelper, wait_for_page_load

class CourseWorkflowTest(unittest.TestCase):

    def setUp(self):
        self.w = get_browser()
        self.student = student_name

        self.docent_lastname = docent_last_name
        self.docent = docent_name

    def test_course_workflow(self):
        self._test_course_creation()
        self._test_course_application()
        self._test_course_deletion()

    def _docent_course_overview(self):
        w = self.w

        LoginPageHelper.perform_login(self.docent)

        menu_dropdown = w.find_element_by_link_text("Lehrveranstaltungen")
        menu_dropdown.click()

        menu_items = w.find_elements_by_link_text("Lehrveranstaltungen")
        assert len(menu_items) == 2, "there should be 2 links called 'Lehrveranstaltungen' ... "
        menu_items[1].click()
        wait_for_page_load()

    def _test_course_creation(self):
        w = self.w

        self._docent_course_overview()

        new_course_button = w.try_find_xpath("//button[contains(text(), '%s')]" % "Neue Lehrveranstaltung anlegen")
        assert new_course_button is not None, "Can't create new course"
        new_course_button.click()
        wait_for_page_load()

        input_name = w.find_element_by_xpath('//input[@id="formName"]')
        input_abb = w.find_element_by_xpath('//input[@id="formabb"]')

        input_name.send_keys("test_course_01")
        input_abb.send_keys("tc1")

        create_course_button = w.try_find_xpath("//button[contains(text(), '%s')]" % "Neue Veransatlung anlegen")
        create_course_button.click()
        wait_for_page_load()

    def _test_course_deletion(self):
        w = self.w

        LoginPageHelper.perform_login(self.docent)

        self._docent_course_overview()

        course_overview_div = w.try_find_xpath("//div[@id='courseOverview']")
        header = course_overview_div.find_element_by_xpath("//div[contains(text(), 'test_course_01')]/..")

        delete_button = header.find_element_by_class_name("fa-trash")
        delete_button.click()

        wait_for_page_load()

        b = w.try_find_xpath("//button[contains(text(), 'Ja')]")
        assert b is not None, "no approve button ('Ja') found in delete-modal"
        b.click()

        wait_for_page_load()

        b = w.try_find_xpath("//button[contains(text(), 'OK')]")
        assert b is not None, "no approve button ('OK') found in delete-modal"
        b.click()

        header = w.try_find_xpath("//div[contains(text(), 'test_course_01')]")
        assert header is None


    def _test_course_application(self):
        w = self.w

        LoginPageHelper.perform_login(self.student)

        menu_entry = w.try_find_linktext("Lehrveranstaltungsanmeldung")
        assert menu_entry is not None
        menu_entry.click()
        wait_for_page_load()

        appl = w.try_find_xpath("//div[contains(text(), '%s')]" % self.docent_lastname)
        assert appl is not None, "can't find docent %s in the overview" % self.docent_lastname
        appl.click()
        wait_for_page_load()

        course = w.find_element_by_xpath("//*[text()='test_course_01']")
        row = course.find_element_by_xpath(".//..//..")
        apply = row.find_element_by_xpath(".//button[text()='%s']" % "Anmelden")

        assert apply is not None, "can't find course to apply to for this docent"
        apply.click()
        wait_for_page_load()

        appl = w.try_find_xpath("//div[contains(text(), '%s')]" % "Angemeldet")
        assert appl is not None, "cant find 'Angemeldet' in menu"
        appl.click()
        wait_for_page_load()

        # w.find_element_by_xpath("*[contains(text(), '%s')]" % "test_course_01")
        course = w.find_element_by_xpath("//*[text()='test_course_01']")
        # row = course.find_element_by_xpath(".//..//..")


# todo: test this a bit more, to make sure all buttons are found ...
class RestrictedCourseWorkflowTest(unittest.TestCase):

    def setUp(self):
        self.w = get_browser()
        self.student = student_name

        self.docent_lastname = docent_last_name
        self.docent = docent_name

    def test_course_workflow(self):
        self._test_course_creation()
        self._test_course_application()

        self._test_course_deletion()

    def _docent_course_overview(self, no_login = False):
        w = self.w

        if no_login is False:
            LoginPageHelper.perform_login(self.docent)

        menu_dropdown = w.find_element_by_link_text("Lehrveranstaltungen")
        menu_dropdown.click()

        menu_items = w.find_elements_by_link_text("Lehrveranstaltungen")
        assert len(menu_items) == 2, "there should be 2 links called 'Lehrveranstaltungen' ... "
        menu_items[1].click()
        wait_for_page_load()

    def _test_course_creation(self):
        w = self.w

        self._docent_course_overview()

        new_course_button = w.try_find_xpath("//button[contains(text(), '%s')]" % "Neue Lehrveranstaltung anlegen")
        assert new_course_button is not None, "Can't create new course"
        new_course_button.click()
        wait_for_page_load()

        input_name = w.find_element_by_xpath('//input[@id="formName"]')
        input_abb = w.find_element_by_xpath('//input[@id="formabb"]')

        input_name.send_keys("test_course_01")
        input_abb.send_keys("tc1")

        span = w.find_element_by_xpath("//span[contains(text(), 'freigeschaltet werden')]")
        span.click()

        create_course_button = w.try_find_xpath("//button[contains(text(), '%s')]" % "Neue Veransatlung anlegen")
        create_course_button.click()
        wait_for_page_load()

    def _test_course_deletion(self):
        w = self.w
        self._docent_course_overview(no_login=True)

        course_overview_div = w.try_find_xpath("//div[@id='courseOverview']")
        header = course_overview_div.find_element_by_xpath("//div[contains(text(), 'test_course_01')]/..")

        delete_button = header.find_element_by_class_name("fa-trash")
        delete_button.click()

        wait_for_page_load()

        b = w.try_find_xpath("//button[contains(text(), 'Ja')]")
        assert b is not None, "no approve button ('Ja') found in delete-modal"
        b.click()

        wait_for_page_load()

        b = w.try_find_xpath("//button[contains(text(), 'OK')]")
        assert b is not None, "no approve button ('OK') found in delete-modal"
        b.click()

        header = w.try_find_xpath("//div[contains(text(), 'test_course_01')]")
        assert header is None


    def _test_course_acception(self):
        w = self.w

        self._docent_course_overview()

        course_overview_div = w.try_find_xpath("//div[@id='courseOverview']")
        header = course_overview_div.find_element_by_xpath("//div[contains(text(), 'test_course_01')]/..")

        user_button = header.find_element_by_class_name("fa fa-user")
        user_button.click()

        wait_for_page_load()


        pending_students_header = w.find_element_by_xpath("//*[contains(text(), 'Freizuschaltende Bewerber']")
        pending_students_header.click()

        student_checkbox_label = w.find_element_by_xpath("//*[contains(text(), '%s']" % student_last_name)
        student_checkbox_label.click()

        accept_button = w.find_element_by_xpath("//*[contains(text(), '%s']" % "Studenten akzeptieren")
        accept_button.click()



    def _test_course_application(self):
        w = self.w

        LoginPageHelper.perform_login(self.student)

        menu_entry = w.try_find_linktext("Lehrveranstaltungsanmeldung")
        assert menu_entry is not None
        menu_entry.click()
        wait_for_page_load()

        appl = w.try_find_xpath("//div[contains(text(), '%s')]" % self.docent_lastname)
        assert appl is not None, "can't find docent %s in the overview" % self.docent_lastname
        appl.click()
        wait_for_page_load()

        course = w.find_element_by_xpath("//*[text()='test_course_01']")
        row = course.find_element_by_xpath(".//..//..")
        apply = row.find_element_by_xpath(".//button[text()='%s']" % "Anmelden")

        assert apply is not None, "can't find course to apply to for this docent"
        apply.click()
        wait_for_page_load()

        appl = w.try_find_xpath("//div[contains(text(), '%s')]" % "Auf Anmeldung warten")
        assert appl is not None, "cant find 'Auf Anmeldung warten' in menu"
        appl.click()
        wait_for_page_load()

        # w.find_element_by_xpath("*[contains(text(), '%s')]" % "test_course_01")
        course = w.find_element_by_xpath("//*[text()='test_course_01']")
        # row = course.find_element_by_xpath(".//..//..")
