import atexit, os
import threading
from time import sleep
from inspect import currentframe,getfile

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

local = threading.local()
standard_timeout = 5

def get_browser():
    """Get a singleton browser that is recycled during tests"""
    def shutdown_browser(handle):
        handle.close()

    def create_browser():
        driver = webdriver.Chrome(os.path.dirname(os.path.abspath(getfile(currentframe()))) + '/../chromedriver')
        # driver.implicitly_wait(0.125)
        driver.set_window_size(1400, 1080)
        return driver

    browser = getattr(local, "__selenium_test_browser", None)

    if not browser:
        browser = create_browser()
        setattr(local, "__selenium_test_browser", browser)
        atexit.register(shutdown_browser, browser)

    browser.get("data:,")

    return browser

def wait_for_page_load():
    sleep(1)
    browser = getattr(local, "__selenium_test_browser", None)
    WebDriverWait(browser, standard_timeout).until(
        lambda driver: driver.find_element_by_tag_name("body"))



class LoginPageHelper:

    @staticmethod
    def perform_login(username:str):
        w = getattr(local, "__selenium_test_browser", None)
        w.get("http://dev.asb.cloudns.org/asbuigwt/")
        w.implicitly_wait(0.75)
        wait_for_page_load()

        username_field = w.find_element_by_xpath('//input[@placeholder="Bitte geben Sie Ihren Benutzernamen an"]')
        username_field.send_keys(username)

        login_button = w.find_element_by_xpath('//button[contains(text(), "%s")]' % "Anmelden")
        login_button.click()
        w.implicitly_wait(0.0625)
        wait_for_page_load()
