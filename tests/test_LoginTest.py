import unittest
import tests.selenium_monkeypatch
from tests.ASB_constants import student_name, docent_name

from tests.selenium_utils import get_browser, wait_for_page_load, LoginPageHelper
from selenium.common.exceptions import NoSuchElementException

class LoginTest(unittest.TestCase):
    def setUp(self):
        self.w = get_browser()

        self.student = student_name
        self.docent = docent_name
        self.invalidLogin1 = "SomeuserThatdoesnotexist"
        self.invalidLogin2 = "SomeÜngültigeCredent14ls"

    def test_student_login(self):
        """
        In meiner Eigenschaft als Student, möchte ich
            - mich einloggen
            - mich danach abmelden
        """

        w = self.w
        LoginPageHelper.perform_login(self.student)

        menu_entry1 = w.try_find_linktext("Lehrveranstaltungsanmeldung")
        assert menu_entry1 is not None, "No menu entry 'Lehrveranstaltungsameldung'. User does not see a valid student-view"
        menu_entry2 = w.try_find_linktext("Aufgaben")
        assert menu_entry2 is not None, "No menu entry 'Aufgaben'. User does not see a valid student-view"

        logout_button = w.find_element_by_xpath('//a[contains(text(), "%s")]' % "Abmelden")
        assert logout_button is not None, "logout button nicht gefunden"
        logout_button.click()

        wait_for_page_load()

    def test_docent_login(self):
        """
        In meiner Eigenschaft als Dozent, möchte ich
            - mich einloggen
            - mich danach abmelden
        """

        w = self.w
        LoginPageHelper.perform_login(self.docent)

        logout_button = w.find_element_by_xpath('//a[contains(text(), "%s")]' % "Abmelden")
        assert logout_button is not None, "logout button nicht gefunden"


        menu_dropdown = w.try_find_linktext("Lehrveranstaltungen")
        assert menu_dropdown is not None, "menu entry 'Lehrveranstaltungen' not found. User does not see a valid docent page"
        # menu_dropdown = w.find_element_by_link_text("Lehrveranstaltungen")
        menu_dropdown.click()

        menu_items = w.find_elements_by_link_text("Lehrveranstaltungen")
        assert len(menu_items) == 2, "there should be 2 links called 'Lehrveranstaltungen' ... "


        logout_button.click()
        wait_for_page_load()

    def test_invalid_login(self):
        """
        In meiner Eigenschaft als Hochschulfremde person, möchte ich
            - versuchen mich anzumelden
            - dies soll nicht gelingen
        """

        w = self.w

        def try_invalid_login(credentials):
            LoginPageHelper.perform_login(credentials)

            logout_button = w.try_find_xpath('//a[contains(text(), "%s")]' % "Abmelden")
            assert logout_button is None, "logout button found after testing invalid credentials '%s'" % credentials

            error_label = w.find_element_by_xpath('//*[contains(text(), "%s")]' % "Anmeldung fehlgeschlagen.")
            assert error_label is not None, "No error is shown for invalid credentials '%s'" % credentials

        try_invalid_login(self.invalidLogin1)

        try_invalid_login(self.invalidLogin2)

        try_invalid_login("")



