import unittest
from time import sleep

import tests.selenium_monkeypatch
from tests.ASB_constants import student_name

from tests.selenium_utils import get_browser, wait_for_page_load


class LanguageTest(unittest.TestCase):
    def setUp(self):
        self.w = get_browser()
        self.student = student_name


    def test_switch_to_english(self):
        w = self.w
        w.get("http://dev.asb.cloudns.org/asbuigwt/")
        wait_for_page_load()
        w.implicitly_wait(0.5)

        m = w.find_element_by_link_text("Anwendungssprache")
        m.click()

        e = w.find_element_by_xpath("//span[contains(text(), 'Deutsch')]")
        i = e.find_element_by_xpath("//../input")  # checkbox for German

        assert i.get_attribute('value') == "on", "got '%s', expected 'on'" % i.get_attribute('value')

        e = w.find_element_by_xpath("//span[contains(text(), 'Englisch')]")
        i2 = e.find_element_by_xpath("//../input")  # checkbox for English

        b = e.find_element_by_xpath("//../..//label")

        b.click()
        sleep(0.125)
        # wait_for_page_load()

        input() # TODO: find the reason why the element cant be clicked ...

        assert i.get_attribute('value') != "on", "got '%s', expected not 'on'" % i.get_attribute('value')
        assert i2.get_attribute('value') == "on", "got '%s', expected 'on'" % i2.get_attribute('value')
