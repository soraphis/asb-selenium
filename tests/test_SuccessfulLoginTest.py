import unittest
import tests.selenium_monkeypatch
from tests.ASB_constants import docent_name
from tests.ASB_constants import student_name

from tests.selenium_utils import get_browser, LoginPageHelper, wait_for_page_load


class SuccessfulLoginTest(unittest.TestCase):
    """
    Diese Klasse enthällt tests, welche prüfen ob die Websockets keine fehler erzeugen,
    d.h. die Seite nach dem login tatsächlich korrekt geladen wird.
    """

    def setUp(self):
        self.w = get_browser()

        self.student = student_name
        self.docent = docent_name
        self.tries = 5 # maximum number of re-login tries

    def test_student_courses(self):
        """
        In meiner Eigenschaft als Student, möchte ich
            - Eine Liste der Kurse sehen, zu denen ich angemeldet bin
            - Eine Liste der Abgaben sehen, die noch einzureichen sind
            - Abgaben einreichen
        """

        w = self.w

        # todo: login as docent, create a new course
        # todo: login as student and enter this course
        # todo: login as docent and enter accept the student
        # todo:       as docent, add a new assignment


        # gbo_course = None
        failed_atempts = 0

        while True:
            LoginPageHelper.perform_login(self.student)

            assert failed_atempts < self.tries, \
                str(self.test_student_courses.__doc__) + "\n html elements could are not loaded correctly"

            log = w.get_log('browser')
            if any(x for x in log if
                   "WebSocket is already in CLOSING or CLOSED state" in x['message']):
                print("failed websocket " + failed_atempts)
                failed_atempts += 1
                continue

            # checks if GBO is visible in the course list ("open tasks")
            # try:
            #     gbo_course = w.find_element_by_partial_link_text("Grafische")
            # except:
            #     failed_atempts += 1
            #     continue

            # checks if there is something under "open reviews"
            try:
                w.find_element_by_xpath("//div[@id='courseAndReviewList']/span")
            except:
                failed_atempts += 1
                continue

            try:
                w.find_element_by_partial_link_text("Lehrveranstaltungen (Student)")
            except:
                failed_atempts += 1
                continue

            try:
                w.find_element_by_partial_link_text("Übungsblätter")
            except:
                failed_atempts += 1
                continue

            break  # everything worked exit the loop

        if failed_atempts > 0:
            print("[WARNING] - needed to relogin %d times until it worked" % failed_atempts)

        # gbo_course.click()
        # wait_for_page_load()
        #
        # # assign_button = w.find_element_by_xpath('//button[contains(text(), "%s")]' % "einreichen")
        # assign_button = w.find_element_by_css_selector('button.btn.btn-default>i.fa.fa-arrow-right') \
        #     .find_element_by_xpath('./..')
        # assign_button.click()
        #
        # wait_for_page_load()
        #
        # # todo: upload something to the assignment
        # #       find input of type "file" and add path to
        #
        # w.find_element_by_xpath('//input[@type="file"]').send_keys("/home/oliver/batery.log")
        # w.find_element_by_xpath('//button[contains(text(), "%s")]' % "Hochladen").click()
        # wait_for_page_load()

    def test_docent_create_course(self):
        w = self.w
        failed_atempts = 0
        while True:
            LoginPageHelper.perform_login(self.docent)

            assert failed_atempts < self.tries, "html elements could are not loaded correctly"

            menu_dropdown = w.find_element_by_link_text("Lehrveranstaltungen")
            menu_dropdown.click()

            menu_items = w.find_elements_by_link_text("Lehrveranstaltungen")
            assert len(menu_items) == 2, "there should be 2 links called 'Lehrveranstaltungen' ... "

            menu_items[1].click()
            wait_for_page_load()

            log = w.get_log('browser')

            if any(x for x in log if
                   "WebSocket is already in CLOSING or CLOSED state" in x['message']):
                failed_atempts += 1
                continue

            # todo: assert len(log) == 0 (there should not be warnings to this point ...)

            break

        if failed_atempts > 0:
            print("[WARNING] - needed to relogin %d times until it worked" % failed_atempts)

            # input()