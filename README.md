# how to run the tests

- install python3
- install virtualenv
- create a virtualenv:  `virtualenv -p python3 venv`
- activate virtualenv: `source ./venv/bin/activate` 
- install virtualenv requirements: `pip install requirements.txt`
- run tests: `python -m unittest discover `



run specific tests:

    python -m unittest discover -p '*Language*'      